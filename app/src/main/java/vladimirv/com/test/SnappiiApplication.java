package vladimirv.com.test;

import android.app.Application;
import android.os.StrictMode;
import android.support.v7.app.AppCompatDelegate;

import vladimirv.com.test.network.ApiInterface;
import vladimirv.com.test.network.RetrofitClient;

/**
 * Created by Vladimir Varavin on 03.10.18.
 */
public class SnappiiApplication extends Application {
    private static SnappiiApplication instance;
    private ApiInterface apiInterface;

    public static ApiInterface apiInterface() {
        return instance.apiInterface;
    }

    public static SnappiiApplication getApplication() {
        return instance;
    }

    @Override
    public void onCreate() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        super.onCreate();
        instance = this;
        apiInterface = RetrofitClient.getInstance().getClient().create(ApiInterface.class);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }
}
