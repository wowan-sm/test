package vladimirv.com.test;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import vladimirv.com.test.model.User;

/**
 * Created by Vladimir Varavin on 04.10.18.
 */
public class Utils {
    private static final String RANDOM_USER_ADDRESS = "https://randomuser.me";

    public static boolean isCallingSupported(Intent intent) {
        boolean result = true;
        PackageManager manager = SnappiiApplication.getApplication().getPackageManager();
        List<ResolveInfo> infos = manager.queryIntentActivities(intent, 0);
        if (infos.size() <= 0) {
            result = false;
        }
        return result;
    }

    public static Uri getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file =  new File(SnappiiApplication.getApplication().getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public static void sendEmail(final Context context, final User user, boolean needAttach){
        final Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{user.getEmail()});
        if (needAttach){
            Glide.with(context).as(Bitmap.class).load(user.getPicture().getLarge()).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                    intent.putExtra(Intent.EXTRA_TEXT, user.toString());
                    intent.putExtra(Intent.EXTRA_STREAM, Utils.getLocalBitmapUri(resource));
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    context.startActivity(Intent.createChooser(intent, "Share contact..."));                }
            });
        }else{
            context.startActivity(Intent.createChooser(intent, "Send mail..."));
        }
    }
}
