package vladimirv.com.test.network;

import java.security.cert.CertificateException;
import java.util.Collections;
import java.util.HashMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static final String RANDOM_USER_ADDRESS = "https://randomuser.me";

    public static final HashMap<String, String> FIELD_QUERY = new HashMap<String, String>() {
        {
            put("inc", "name,location,email,phone,picture,nat");
        }
    };
    private static final RetrofitClient INSTANCE = new RetrofitClient();
    private Retrofit retrofit;

    public static RetrofitClient getInstance() {
        return INSTANCE;
    }

    public Retrofit getClient() {
        if (retrofit == null) {
            ConnectionSpec spec = new
                    ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                    .allEnabledTlsVersions()
                    .allEnabledCipherSuites()
                    .build();
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectionSpecs(Collections.singletonList(spec))
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(RANDOM_USER_ADDRESS)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
