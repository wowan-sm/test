package vladimirv.com.test.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import vladimirv.com.test.R;
import vladimirv.com.test.activity.UserDetailPage;
import vladimirv.com.test.model.Name;
import vladimirv.com.test.model.Picture;
import vladimirv.com.test.model.User;

/**
 * Created by Vladimir Varavin on 03.10.18.
 */
public class UsersListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<User> users;

    public UsersListAdapter(List<User> users) {
        this.users = users;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_item, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (users != null && users.size() != 0) {
            User user = users.get(position);
            if (holder instanceof UserViewHolder) {
                UserViewHolder userViewHolder = (UserViewHolder) holder;
                userViewHolder.setUser(user);
            }
        }
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {

        private View mainView;
        private ImageView userImageView;
        private TextView nameView;

        void setUser(final User user){
            String userName = "";
            Name name = user.getName();
            if (name != null){
                userName = name.toString();
            }
            userName = userName.isEmpty() ? "Unknown" : userName;
            nameView.setText(userName);

            Picture picture = user.getPicture();
            if (picture != null) {
                String thumbnail = picture.getThumbnail();
                if (thumbnail != null) {
                    Glide.with(mainView).load(user.getPicture().getThumbnail()).into(userImageView);
//                    Picasso picasso = new Picasso.Builder(SnappiiApplication.getApplication())
//                            .indicatorsEnabled(true)
//                            .loggingEnabled(true)
//                            .downloader(new OkHttp3Downloader(new OkHttpClient()))
//                            .build();
//
//                    picasso.load(thumbnail).into(new Target() {
//                        @Override
//                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                            userImageView.setImageBitmap(bitmap);
//                            // edit your bitmap and set as background
//                            BitmapDrawable ob = new BitmapDrawable(SnappiiApplication.getApplication().getResources(), bitmap);
//                            userImageView.setImageDrawable(ob);
//                        }
//
//                        @Override
//                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {
//                        }
//
//                        @Override
//                        public void onPrepareLoad(Drawable placeHolderDrawable) {
//                            userImageView.setImageDrawable(AppCompatResources.getDrawable(SnappiiApplication.getApplication(), R.drawable.user_face));
//                        }
//                    });
                }
            }
            mainView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, UserDetailPage.class);
                    intent.putExtra("user", user);
                    context.startActivity(intent);
                }
            });
        }

        UserViewHolder(View view){
            super(view);
            mainView = view;
            userImageView = view.findViewById(R.id.userImageView);
            nameView = view.findViewById(R.id.userNameTextView);
        }
    }
}
