package vladimirv.com.test.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Vladimir Varavin on 03.10.18.
 */
public class Picture implements Serializable {

    @SerializedName("large")
    @Expose
    private String large;

    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;

    public String getLarge() {
        return large;
    }

    public String getThumbnail() {
        return thumbnail;
    }
}