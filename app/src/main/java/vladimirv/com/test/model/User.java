package vladimirv.com.test.model;

/**
 * Created by Vladimir Varavin on 03.10.18.
 */

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User implements Serializable {

    @SerializedName("name")
    @Expose
    private Name name;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("picture")
    @Expose
    private Picture picture;
    @SerializedName("nat")
    @Expose
    private String nat;

    public Name getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public Picture getPicture() {
        return picture;
    }

    public String getNat() {
        return nat;
    }

    @Override
    public String toString() {
        return TextUtils.join("\n", new String[]{
                "Name: " + name.toString(),
                "Email: " + email,
                "Phone: " + phone,
                "Nationality: " + nat,
                "Address: " + location.toString()
        });
    }
}