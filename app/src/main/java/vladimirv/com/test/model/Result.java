package vladimirv.com.test.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Vladimir Varavin on 03.10.18.
 */
public class Result {

    @SerializedName("results")
    @Expose
    private List<User> results = null;

    public List<User> getUsers() {
        return results;
    }
}
