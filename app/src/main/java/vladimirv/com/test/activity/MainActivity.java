package vladimirv.com.test.activity;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vladimirv.com.test.R;
import vladimirv.com.test.SnappiiApplication;
import vladimirv.com.test.adapters.UsersListAdapter;
import vladimirv.com.test.model.Result;
import vladimirv.com.test.model.User;
import vladimirv.com.test.network.RetrofitClient;

public class MainActivity extends SnapiiActivity {

    private static final int COUNT_USER_LOAD = 50;
    private static final int STEP_COUNT_USER_LOAD = 20;
    private int totalItemCount = 0;

    private List<User> userList;
    private UsersListAdapter usersListAdapter;

    @BindView(R.id.userRecyclerView)
    RecyclerView userRecyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        userList = new ArrayList<>();
        setupRecyclerView();
        loadData();
    }

    private void loadData() {
        HashMap<String, String> stringHashMap = new HashMap<>();
        stringHashMap.put("results", String.valueOf(COUNT_USER_LOAD));
        stringHashMap.putAll(RetrofitClient.FIELD_QUERY);

        SnappiiApplication.apiInterface().getUsers(stringHashMap).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.body() != null) {
                    List<User> users = response.body().getUsers();
                    if (users != null) {
                        if (userList.isEmpty()) {
                            userList.addAll(users);
                            usersListAdapter = new UsersListAdapter(userList);
                            userRecyclerView.setAdapter(usersListAdapter);
                        } else {
                            userList.addAll(users);
                        }
                        userRecyclerView.post(new Runnable() {
                            @Override
                            public void run() {
                                usersListAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Error incoming data", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setupRecyclerView() {
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        userRecyclerView.setLayoutManager(mLayoutManager);
        userRecyclerView.setNestedScrollingEnabled(false);
        final Drawable divider = getResources().getDrawable(R.drawable.divider);
        userRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                int left = parent.getPaddingLeft();
                int right = parent.getWidth() - parent.getPaddingRight();

                int childCount = parent.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    View child = parent.getChildAt(i);
                    RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                    int top = child.getBottom() + params.bottomMargin;
                    int bottom = top + divider.getIntrinsicHeight();

                    divider.setBounds(left, top, right, bottom);
                    divider.draw(c);
                }
            }
        });
        userRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = mLayoutManager.getItemCount();
                int lastVisibleItem = ((LinearLayoutManager) mLayoutManager).findLastVisibleItemPosition();
                if (lastVisibleItem >= totalItemCount - STEP_COUNT_USER_LOAD) {
                    loadData();
                }
            }
        });
    }
}
