package vladimirv.com.test.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.AppCompatImageView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import vladimirv.com.test.R;
import vladimirv.com.test.Utils;
import vladimirv.com.test.model.User;

@RuntimePermissions
public class UserDetailPage extends SnapiiActivity {

    private User user;

    @BindView(R.id.userIcon)
    AppCompatImageView userIcon;
    @BindView(R.id.nameTextView)
    TextView nameTextView;
    @BindView(R.id.emailTextView)
    TextView emailTextView;
    @BindView(R.id.emailImageView)
    ImageView emailImageView;
    @BindView(R.id.phoneTextView)
    TextView phoneTextView;
    @BindView(R.id.callImageView)
    ImageView callImageView;
    @BindView(R.id.natTextView)
    TextView natTextView;
    @BindView(R.id.addressTextView)
    TextView addressTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail_page);
        user = (User) getIntent().getExtras().getSerializable("user");
        if (user != null) {
            Glide.with(this).load(user.getPicture().getLarge()).into(userIcon);
            String nameString = user.getName().toString();
            String email = user.getEmail();
            String phone = user.getPhone();
            nameTextView.setText(nameString.isEmpty() ? "Unknown" : nameString);
            emailTextView.setText(email);
            phoneTextView.setText(phone);
            natTextView.setText(user.getNat());
            addressTextView.setText(user.getLocation().toString());
            emailImageView.setVisibility(email == null || email.isEmpty() ? View.INVISIBLE : View.VISIBLE);
            callImageView.setVisibility(phone == null || phone.isEmpty() ? View.INVISIBLE : View.VISIBLE);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick({R.id.emailImageView, R.id.callImageView})
    public void onViewClicked(View view) {
        if (user != null) {
            switch (view.getId()) {
                case R.id.emailImageView:
                    if (user.getEmail() != null && !user.getEmail().isEmpty()) {
                        Utils.sendEmail(this, user, false);
                    }
                    break;
                case R.id.callImageView:
                    UserDetailPagePermissionsDispatcher.callWithPermissionCheck(this);
                    break;
            }
        }
    }

    @NeedsPermission(Manifest.permission.CALL_PHONE)
    void call() {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + user.getPhone()));
        if (Utils.isCallingSupported(intent)) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            startActivity(intent);
        } else {
            Toast.makeText(this, "This device don`t support phone call", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        UserDetailPagePermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_share:
                Utils.sendEmail(this, user, true);
                break;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }
}
